import React from 'react';

import "./Button.css";

export default function Button({ title = "something", handleClick, disabledLogin }) {
    return (
        <button disabled={disabledLogin} onClick={handleClick}>{title}</button>
    );
}
