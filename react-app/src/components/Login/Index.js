import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

import Button from '../Button/Index';
import '../Login/Login.css';

export default function Login(props ){
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)
    const [emailError, setEmailError] = useState('Емейл не может быть пустым')
    const [passwordError, setPasswordError] = useState('Пароль не может быть пустым')
    const [formValid, setFormValid] = useState('Заполните форму')

    useEffect(() => {
        if (emailError || passwordError) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    }, [emailError, passwordError])

    const blurHandler = (e) => {
        switch (e.target.name) {
            case 'email':
                setEmailDirty(true)
                break
            case 'password':
                setPasswordDirty(true)
                break
        }
    }

    const emailHandler = (e) => {
        setEmail(e.target.value)
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(e.target.value).toLowerCase())) {
            setEmailError('Некорректный email')
        } else {
            setEmailError('')
        }
    }

    const passwordHadler = (e) => {
        setPassword(e.target.value)
        if (e.target.value.length < 8) {
            setPasswordError('Пароль должен быть длинее 8 символов')
            if (!e.target.value) {
                setPasswordError('Пароль не может быть пустым')
            }
        } else {
            setPasswordError('')
        }
    }


    return (
        <div className="login-container background">
            <form action="" className="login-container__form" >

                <div className="login-container__title">Вход</div>
                {(emailDirty && emailError) && <div className="login-container-error">{emailError}</div>}
                <input value={email} onChange={emailHandler} onBlur={e => blurHandler(e)} className="login-container__input" name="email" type="text" placeholder='Введите ваш емейл' />
                {(passwordDirty && passwordError) && <div className="login-container-error">{passwordError}</div>}
                <input value={password} onChange={passwordHadler} onBlur={e => blurHandler(e)} className="login-container__input" name="password" type="password" placeholder='Введите ваш пароль'/>
                <div className={"agreement"}>
                    <input className="login-container__cheсkbox" type="checkbox" id="agreement"/>
                    <div className="login-container__cheсkbox-mark"></div>
                    <label for="agreement">Я согласен получать обновления на почту</label>
                </div>
                <Link to={"/cards"}>
                    <Button disabledLogin={!formValid} title={"Войти"} />
                </Link>
            </form>
        </div>
    );
}




