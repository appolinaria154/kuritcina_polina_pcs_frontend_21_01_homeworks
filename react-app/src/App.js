import { Routes, Route } from 'react-router-dom';

import LoginPage from './page/LoginPage/LoginPage';
import CardPage from './page/CardPage/index';
import Index from './page/BasketPage/index';

import './App.css';

function App() {
    return (
        <Routes>
            <Route path={"/"} element={<LoginPage/>} />
            <Route path={"/cards"} element={<CardPage/>} />
            <Route path={"/basket"} element={<Index/>} />
        </Routes>
    );
}

export default App;


