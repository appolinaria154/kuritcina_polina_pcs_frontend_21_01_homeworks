import React from 'react';

import Login from '../../components/Login/Index'

import './LoginPage.css';

export default function LoginPage() {
    return (
        <div className={"login-page"}>
            <Login/>
        </div>
    );
}
