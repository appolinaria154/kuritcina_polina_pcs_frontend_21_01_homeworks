import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import Card from "../../components/Card/Card";
import "../CardPage/CardPage.css";

export default function CardsPage(props) {
    const [cardsData, setCardsData] = useState([]);
    const [basketData, setBasketData] = useState(JSON.parse(localStorage.getItem('basketData')) || []);
    const [count, setCount] = useState(basketData?.length);
    const [price, setPrice] = useState(0);

    function addFood(food) {
        setBasketData([...basketData, food])
    }

    useEffect(() => {
        console.log(basketData)
        let result = basketData.reduce(function(accumulator, currentValue) {
            return accumulator + Number(currentValue.price);
        }, 0);
        setPrice(result);
        setCount(basketData?.length);
        localStorage.setItem('basketData', JSON.stringify(basketData));
    });

    // get data by fetch
    useEffect(() => {
        fetch(`https://alinagaripova.github.io/json-api/food.json`)
            .then(response => {
                if (!response.ok) {
                    throw new Error("HTTP error " + response.status);
                }
                return response.json();
            })
            .then(data => {
                console.log(data)
                setCardsData(data.data);
            })
            .catch(error => console.error("food.json loader", error));
    }, []);

    console.log(cardsData)

    return (
        <div className="cards-container">
            <div className="header">
                <h1>Наша продукция</h1>
                <Link to={'/basket'}>
                    <div className="basket">
                        {count} шт на сумму {price} ₽
                        <button className='basket-button'/>
                    </div>

                </Link>
            </div>
            <div className="cards-list">
                {cardsData ? cardsData.map(item => {
                    return (
                        <Card
                            item={item}
                            addFood={addFood}
                        />)
                }) : null}
            </div>
        </div>
    );
}
